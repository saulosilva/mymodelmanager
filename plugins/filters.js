import Vue from 'vue'
import moment from 'moment'

Vue.filter('yesOrNo', (value) => {
  return value === true ? 'Sim' : 'Não'
})

Vue.filter('genderTrans', (value) => {
  return value === 'MALE' ? 'Masculino' : 'Feminino'
})

Vue.filter('accountType', (value) => {
  return value === 'CC' ? 'Conta-corrente' : 'Conta-poupança'
})

Vue.filter('scholarityTrans', (value) => {
  switch (value) {
    case 'NONE': {
      value = 'Não sabe ler nem escrever'
      break
    }
    case 'ELEMENTARY_INCOMPLETE': {
      value = 'Ensino fundamental incompleto'
      break
    }
    case 'ELEMENTARY': {
      value = 'Ensino fundamental completo'
      break
    }
    case 'HIGH_SCHOOL_INCOMPLETE': {
      value = 'Ensino médio incompleto'
      break
    }
    case 'HIGH_SCHOOL': {
      value = 'Ensino médio completo'
      break
    }
    case 'GRADUATION_INCOMPLETE': {
      value = 'Ensino superior incompleto'
      break
    }
    case 'GRADUATED': {
      value = 'Pós-graduação'
      break
    }
    case 'MASTER': {
      value = 'Ensino superior completo'
      break
    }
    case 'OTHERS': {
      value = 'Outros'
      break
    }
  }
  return value
})

Vue.filter('join', (value, separator = ',') => {
  if (!value) return ''
  if (value.length === 0) return ''

  return value.join(separator)
})

Vue.filter('age', (value) => {
  if (!value) return 0

  var date = moment()
  var ageDate = moment(value)
  var a = moment([date.year(), 1])
  var b = moment([ageDate.year(), 0])

  return a.diff(b, 'years')
})

Vue.filter('birthDateFormat', (value) => {
  if (!value) return '01/01/1900'
  if (value === 'Invalid date') return '01/01/1900'

  var date = moment(value, 'YYYY-MM-DD')

  return date.format('DD/MM/YYYY')
})

Vue.filter('dateFormat', (value) => {
  let dateReg = /^\d{4}([./-])\d{2}\1\d{2}$/
  if (value === '' || value === undefined) {
    value = ''
  }

  if (value === 'Invalid date') {
    value = ''
  }

  if (value.match(dateReg)) {
    if (!value) return '01/01/1900'
    if (value === 'Invalid date') return '01/01/1900'

    var date = moment(value, 'YYYY-MM-DD')

    return date.format('DD/MM/YYYY')
  }
  return value
})

Vue.filter('modelStatus', (value) => {
  if (!value) return ''

  var _class = ''
  var name = ''
  switch (value) {
    case 'PRE_CANDIDATE': {
      _class = 'badge-dafault'
      name = 'Pré-candidado'
      break
    }
    case 'PENDING': {
      _class = 'badge-alert'
      name = 'Pendente'
      break
    }
    case 'APPROVED': {
      _class = 'badge-success'
      name = 'Aprovado'
      break
    }
    case 'REPROVED': {
      _class = 'badge-danger'
      name = 'Reprovado'
      break
    }
  }
  var span = '<span class="badge ' + _class + '">' + name + '</span>'
  return span
})

Vue.filter('castingStatus', (value) => {
  if (!value) return ''

  var _class = ''
  var _text = ''
  switch (value) {
    case 'PRE_SELECTION': {
      _class = 'label-default'
      _text = 'Pré-seleção'
      break
    }
    case 'USED': {
      _class = 'label-success'
      _text = 'Utilizado'
      break
    }
  }
  var span = '<span class="label ' + _class + '">' + _text + '</span>'
  return span
})
