import Vue from 'vue'

const isMobile = {}

isMobile.install = function (Vue, options) {
  const ismobilejs = require('ismobilejs')

  Object.defineProperty(Vue.prototype, 'isMobile', {
    get () {
      return ismobilejs
    }
  })
}
if (process.browser) {
  Vue.use(isMobile)
}
