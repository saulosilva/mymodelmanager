import { sync } from 'vuex-router-sync'
// import { sync } from 'vuex-router-sync'
export default ({app: {store, router}}) => {
  sync(store, router)
}

// if (process.BROWSER_BUILD) {
//   sync(store, router)
// }
