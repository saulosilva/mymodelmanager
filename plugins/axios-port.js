import { setClient } from '~/plugins/http'
import swal from 'sweetalert'
import { last, forEach } from 'lodash'
// import moment from 'moment'
const camel = require('to-camel-case')

// let formatDate = (value) => {
//   var date = moment(value, 'DD/MM/YYYY')

//   return date.format('YYYY-MM-DD')
// }

export default function ({ $axios, redirect, app }) {
  setClient($axios)

  $axios.onRequest(config => {
    console.log('Making request to ' + config.url)
  })

  $axios.onError(error => {
    const code = parseInt(error.response && error.response.status)

    if (code === 401) {
      redirect('/login')
    }

    if (code === 403) {
      app.$toast.error(error.response.data.message)
    }

    if (code === 429) {
      swal('Erro!', error.response.data.message, 'error')
    }

    if (code === 400) {
      let message = last(error.response.data.errors)
      swal('Erro!', message.message, 'error')
    }

    if (code === 422) {
      let message = last(error.response.data)

      if (message) {
        swal('Erro!', message, 'error')
      }

      if (error.response.data.hasOwnProperty('errors')) {
        let { errors } = error.response.data

        forEach(errors, (messages, field) => {
          let obj = jQuery('[name="' + camel(field) + '"]').filter(':visible').parent()

          obj.removeClass('has-error')
          obj.find('span')
            .remove()

          obj.addClass('has-error')

          messages.forEach(msg => {
            obj.append('<span class="help-block">' + msg + '</span>')
          })

          obj.keyup(() => {
            obj.removeClass('has-error')
            obj.find('span')
              .remove()
          })
        })
        swal('Erro!', 'Ops!!! Verifique todos os campos!', 'error')
      }
    }

    return Promise.reject(error)
  })
}
