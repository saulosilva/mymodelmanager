import Vue from 'vue'
import Paginate from 'vuejs-paginate'

if (process.browser) {
  Vue.component('paginate', Paginate)
}
