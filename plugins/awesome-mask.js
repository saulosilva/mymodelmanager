import Vue from 'vue'
import AwesomeMask from 'awesome-mask'
/**
* This is the AwesomeMask
*/
Vue.directive('mask', AwesomeMask)
