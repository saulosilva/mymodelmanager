var isMobile = require('ismobilejs')
// var _router = {}
// var _store = {}

export const castingJs = () => {
  // set vue-router
  // _router = router
  // _store = store
  $('.add-cast-item').on('click', function (ev) {
    ev.preventDefault()
    var card = $(this).closest('.col-md-6')
    var img = $(this).data('img')
    var nome = $(this).data('nome')
    var item = $("<div class='item' data-toggle='tooltip' data-placement='top' title='" + nome + "'><img src='img/" + img + "'></div>")
    var link = $("<a href='#' class='remove-cast-item' data-id='" + card.attr('id') + "'><span class='fa fa-close'></span></a>")
    // REMOVE CAST ITEM FUNCION
    $(link).click(function () {
      $(this).closest('.item').fadeOut()
      var id = '#' + $(this).data('id')
      $(id).fadeIn()
    })
    // CONTINUE ADD CAST ITEM
    $(item).append(link)
    $(item).tooltip()
    $(item).hide().fadeIn()
    $('.cast-items').append(item)
    $(card).fadeOut()
  })

  $('#clear-casting').click(function () {
    $('.cards .col-md-6').fadeIn()
    $('.cast-items .item').fadeOut(function () {
      $(this).remove()
    })
  })

  // JOB - CASTING SELECTION
  $('#job-casting').on('change', function () {
    if (isMobile.any) {
      $('#casting-list-mobile').fadeIn()
    } else {
      $('#casting-list').fadeIn()
    }
    $('#casting-msg').hide()
  })
}
