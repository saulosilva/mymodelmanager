import Vue from 'vue'
import { focus } from 'vue-focus'

Vue.directive('focus', focus)
