import Vue from 'vue'
import PictureInput from 'vue-picture-input'

if (process.browser) {
  Vue.component('picture-input', PictureInput)
}
