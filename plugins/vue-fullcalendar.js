import Vue from 'vue'
import FullCalendar from 'vue-full-calendar'

if (process.browser) {
  Vue.use(FullCalendar)
  require('fullcalendar/dist/locale/pt-br')
}
