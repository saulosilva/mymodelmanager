// var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)
var isMobile = require('ismobilejs')
const trans = (value) => {
  let labels = [
    { text: 'Ativo', label: '1' },
    { text: 'Inativo', label: '0' },
    { text: 'Financeiro', label: 'ROLE_FINANCE' },
    { text: 'Casting', label: 'ROLE_CASTING' },
    { text: 'New Face', label: 'ROLE_NEWFACE' },
    { text: 'Admin', label: 'ROLE_ADMIN' },
    { text: 'Aprovados', label: 'APPROVED' },
    { text: 'Pré-cadastrados', label: 'PRE_CANDIDATE' },
    { text: 'Pendente', label: 'PENDING' },
    { text: 'Reprovados', label: 'REPROVED' },
    { text: 'Inativos', label: 'INACTIVE' },
    { text: 'Pré-seleção', label: 'PRE_SELECTION' },
    { text: 'Utilizado', label: 'USED' },
    { text: 'Casting', label: 'CASTING' },
    { text: 'Aguardando Aprovação', label: 'WAITING_APPROVAL' },
    { text: 'Concluído', label: 'COMPLETED' },
    { text: 'Cancelado', label: 'CANCELED' }
  ]

  if (labels.find(r => r.label === value) !== undefined) {
    return labels.find(r => r.label === value).text
  }
  return value
}

export const toggleWindow = (btn) => {
  var toggle = '#' + $(btn).data('toggle')
  $(toggle).fadeToggle()
}
export const search = () => {
  // SEARCH GROUP
  $('#btn-search').click(function () {
    toggleWindow(this)
    setTimeout(() => {
      var form = $('#' + $(this).data('toggle')).find('form')
      form.find('input').first().focus()
    }, 200)
  })

  $('#btn-filter').click(function () {
    if (!isMobile.any) {
      var text = ''
      $('#btn-search').text(text)
      $('.search-select').each(function () {
        if ($(this).val() !== '') {
          var val = trans($(this).val())
          text += val + ', '
        }
      })
      $('.search-input').each(function () {
        if ($(this).val() !== '') {
          text += $(this).val() + ', '
        }
      })

      text = text.slice(0, -2) // remove vircula no final do texto
      var searchIcon = $('<span>').addClass('fa fa-search')
      $('#btn-search').append(searchIcon)
      $('#btn-search').append(' ' + text)
    }
    toggleWindow(this)
  })

  $('#btn-clear').click(function () {
    $('.search-input').val('')
    $('.search-select :nth-child(1)').prop('selected', true)
    $('#btn-search').text('')

    var searchIcon = $('<span>').addClass('fa fa-search')
    $('#btn-search').append(searchIcon)
    toggleWindow(this)
  })

  // Fecha o dropdown caso usuário clique fora dele
  $(document).mouseup(function (e) {
    var container = $('.dropdown-search')
    var button = $('#btn-search')
    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0 && !button.is(e.target) && button.has(e.target).length === 0) {
      $(container).fadeOut()
    }
  })
}
