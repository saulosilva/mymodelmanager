import Vue from 'vue'
import progressStatus from '~/components/models/partials/status'

Vue.component('progress-status', progressStatus)

// Include bootstrap JS only
if (process.browser) {
  $(document).ready(function () {
    // TOOLTIP ACTIVATION
    $('[data-toggle="tooltip"]').tooltip()
    $('[data-toggle="popover"]').popover()
  })
}
