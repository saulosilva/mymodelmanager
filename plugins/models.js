import Vue from 'vue'

const Models = {}

Models.install = function (Vue, options) {
  const models = require('~/utils/models.js')

  Object.defineProperty(Vue.prototype, '$models', {
    get () {
      return models
    }
  })
}

if (process.browser) {
  Vue.use(Models)
}
