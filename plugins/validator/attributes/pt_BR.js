export default {
  name: 'nome',
  email: 'e-mail',
  password: 'senha',
  confirmed_password: 'confirmar senha',
  permissionsId: 'perfil',
  fullName: 'nome completo',
  zipCode: 'CEP',
  street: 'rua',
  number: 'número',
  cityBlock: 'bairro',
  state: 'estado',
  city: 'cidade',
  category: 'categoria',
  skinColor: 'cor de pele',
  birthDate: 'data de nascimento'
}
