import Vue from 'vue'
import VeeValidate, { Validator } from 'vee-validate'
import ptbr from 'vee-validate/dist/locale/pt_BR.js'
import attributesPtBR from './validator/attributes/pt_BR.js'

/**
* This is the VueValidator
*/
Validator.localize('pt_BR', ptbr)

Vue.use(VeeValidate, {
  inject: true,
  locale: 'pt_BR',
  dictionary: {
    pt_BR: { attributes: attributesPtBR }
  }
})
