var totalsteps
var _router = {}
var _store = {}
var _validator = {}
var _swal = {}

export const actions = ({ router, store, validator, swal }) => {
  // set vue-router
  _router = router
  _store = store
  _validator = validator
  _swal = swal

  totalsteps = parseInt($('#total-steps').text())

  $(document).on('keypress', function (e) {
    if (e.which === 13) {
      if (validateForm($('.form-group:visible'))) {
        stepUP($('.form-group:visible'))
        e.preventDefault()
      }
    }
  })

  $('#btn-next').on('click', function () {
    // validate form
    if (validateForm($('.form-group:visible'))) {
      stepUP($('.form-group:visible'))
    }
  })

  $('#btn-prev').on('click', function () {
    if (validateForm($('.form-group:visible'))) {
      stepDown($('.form-group:visible'))
    }
  })

  $('.info').on('click', function () {
    $('.resumo').fadeOut(100)
    var target = $(this).data('target')
    stepUP($('#' + target).prev('.form-group'))
  })
}

export const stepUP = (element) => {
  var jumpstep = 0
  var forward = $(element).next('.form-group')
  if (element.hasClass('resumo')) {
    if ($('#btn-next').data('relation')) {
      _store.dispatch('saveRelationWizard', {
        relation: $('#btn-next').data('relation'),
        data: $('#btn-next').data('form')
      }).then(() => {
        _router.push({ path: $('#btn-next').data('next') })
      })
    }

    if (!$('#btn-next').data('relation')) {
      _store.dispatch($('#btn-next').data('action'), $('#btn-next').data('form')).then(() => {
        _router.push({ path: $('#btn-next').data('next') })
      })
    }

    return false
  }
  if (forward.length > 0) {
    if (element.attr('id') === 'passport') {
      if ($('input[name=passport]:checked').val() === '0') {
        forward = forward.next('.form-group')
        jumpstep = 1
      }
    }
    $(element).fadeOut(100)
    $(forward).slideDown(function () {
      $(forward).find('input:first').focus()
    })
    var step = parseInt($('#step').text())
    step = step + 1 + jumpstep
    $('#step').text(step)
    updateProgress(step)
  }
}

export const stepDown = (element) => {
  var prev = $(element).prev('.form-group')

  if (prev.length > 0) {
    $(element).fadeOut(100)
    $(prev).slideDown(function () {
      $(prev).find('input:first').focus()
    })
    var step = parseInt($('#step').text())
    step--
    $('#step').text(step)
    updateProgress(step)
  }
}

export const updateProgress = (step) => {
  var bar = (step / totalsteps) * 100
  $('.progress-bar').css('width', bar + '%')
}

export const validateForm = (element) => {
  if (_validator) {
    let input = element.find('input')
    let select = element.find('select')

    if (input.length === 1 || select.length === 1) {
      let check = (input.attr('aria-required') === 'true' && input.attr('aria-invalid') === 'true') ||
                  (select.attr('aria-required') === 'true' && select.attr('aria-invalid') === 'true')

      if (check) {
        _swal('Erro!', 'Preencha os campos do formulário corretamente.', 'error')
        return false
      }
      return true
    }
  }
  return true
}
