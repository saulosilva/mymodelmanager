export default {
  data: () => ({
    active: 1,
    tabsLength: 3
  }),
  methods: {
    changeActive (num, arrow = null) {
      switch (arrow) {
        case 'up': {
          if (this.active > 1) {
            this.active--
          }
          break
        }
        case 'down': {
          if (this.active < this.tabsLength) {
            this.active++
          }
          break
        }
        default: {
          this.active = num
        }
      }
    }
  }
}
