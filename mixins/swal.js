export default {
  data: () => ({
    title: '',
    text: ''
  }),
  methods: {
    swalDanger (title, text) {
      return this.$swal({
        title: title,
        text: text,
        icon: 'warning',
        buttons: {
          cancel: {
            text: 'Cancelar',
            value: null,
            visible: true,
            closeModal: true
          },
          confirm: {
            text: 'Apagar',
            value: true,
            visible: true,
            closeModal: true
          }
        },
        dangerMode: true
      })
    },
    swalOptions (title, btn1 = 'Sim', btn2 = 'Não') {
      return this.$swal(title, {
        buttons: [btn2, btn1]
      })
    }
  }
}
