export default {
  data: () => ({
    step: 0,
    totalSteps: 0,
    endStep: false,
    loading: false
  }),
  methods: {
    save () {
      if (this.step === this.totalSteps) {
        return true
      }
      return false
    },
    changeStep (step) {
      this.step = step
      this.endStep = true
    },
    checkEndStep () {
      if (this.endStep === true) {
        this.step = this.totalSteps
        this.endStep = false
        return true
      }
      return false
    },
    stepUp () {
      if (this.save() === false && this.checkEndStep() === false) {
        this.step += 1
      }
    },
    stepDown () {
      if (this.step > 0) {
        this.step -= 1
        this.endStep = false
      }
    }
  }
}
