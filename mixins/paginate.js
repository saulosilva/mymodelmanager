import { drop } from 'lodash'
export default {
  data: () => ({
    pageNum: 1,
    perPage: 10
  }),
  computed: {
    items () {
      return []
    },
    totalPages () {
      if (this.items) {
        return Math.ceil(this.items.length / this.perPage)
      }
      return 0
    },
    total () {
      return this.items.length
    },
    paginateItems () {
      const offset = (this.pageNum - 1) * this.perPage
      const paginateItems = drop(this.items, offset).slice(0, this.perPage)

      return paginateItems
    },
    isVisible () {
      return this.totalPages > 1
    }
  },
  methods: {
    getPaginatedItems (pageNum) {
      this.pageNum = pageNum
    }
  }
}
