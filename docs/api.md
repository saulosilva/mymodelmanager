# Resume
https://mymm-api.herokuapp.com/ (URL Api Mock)

https://mmmapi.herokuapp.com/ (URL Api Production)

_Not need: Authorization Bearer {token}_

_Only: Authorization {token}_

# Methods

# AUTH

**Request:**
```code
HTTP POST /auth
```

**BODY**
``` json

{
  "username" : "user",
  "password" : "password"
}
```
**Response:**
``` json
{
  "token" : "user"
}
```

# GET USER

```code
HTTP POST /user
```

**Response:**

Informacões do usuário em formato json incluindo as ROLES para construcão do menu.

# UPDATE AGENCY
**Request:**
```code
HTTP POST /user/agency
```

**BODY**
``` json
{
  "email" : "youmodels@youmodelsnewemail.com"
}
```
**Response:**

Conterá todos os dados da empresa após a alteração:
``` json
{
  "id" : 1,
  "name" : "YouModels",
  "razaoSocial" : "Razao Social",
  "cnpj" : "cnpj",
  "ie" : "ie",
  "site" : "www.youmodels.com.br",
  "email" : "youmodels@youmodelsnewemail.com"
}
```