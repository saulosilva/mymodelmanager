$(document).ready(function(){
var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;

$('#entrar').click(function(){
  $('#email').addClass('error');
  $('#usuario').addClass('error');
  $('#email').val('');
  $('#senha').addClass('error');
  $('#password').addClass('error');
  $('#password').val('');
  $('#error-msg').removeClass('hidden');
});

function removeInalidaMsg(){
  $('#email').removeClass('error');
  $('#usuario').removeClass('error');
  $('#senha').removeClass('error');
  $('#password').removeClass('error');
  $('#error-msg').addClass('hidden');
}

$('#email').focus(removeInalidaMsg);
$('#password').focus(removeInalidaMsg);

$('.error-msg').blur(function(){
  var input = $(this);
  if(!input.val()){
    input.addClass('error');
    input.prev('label').addClass('error');
    if(input.next('span').length == 0){
      input.after('<span class="error">Campo obrigatório</span>')
    }
    input.focus();
  }
});

$('.error-msg').keyup(function(){
  var input = $(this);
  if(input.hasClass('error')){
    input.removeClass('error');
    input.prev('label').removeClass('error');
    input.next('span').remove();
  }
});

//TOOLTIP ACTIVATION
$('[data-toggle="tooltip"]').tooltip();
$('[data-toggle="popover"]').popover();

//BUTTON LINK
$('button').click(function(ev){
  ev.preventDefault();
  $(location).attr('href', $(this).attr('href'));
});

//TOGGLE-FORM
$('.toggle-form').click(function(ev){
  ev.preventDefault();
  var target = $(this).data('target');
  toggleForm(target, $(this));
});

$('.arrow-down').click(function(ev){
  ev.preventDefault();
  var next = false;
  $('.toggle-form').each(function(){
    if(next){
      var target = $(this).data('target');
      toggleForm(target,$(this));
      return false;
    }
    if($(this).hasClass('active')){
      next = true;
    }
  });
});

$('.arrow-up').click(function(ev){
  ev.preventDefault();
  var next = false;
  jQuery.fn.reverse = [].reverse;
  $('.toggle-form').reverse().each(function(){
    if(next){
      var target = $(this).data('target');
      toggleForm(target,$(this));
      return false;
    }
    if($(this).hasClass('active')){
      next = true;
    }
  });
});

function toggleForm(target, button){
  $('.collapsable').removeClass('active');
  $('#'+target).addClass('active');
  $('.toggle-form').removeClass('active');
  $(button).addClass('active');
}

//FORMATAR MASCARAS DE CAMPOS
function formatar(mascara, documento){
  var i = documento.value.length;
  var saida = mascara.substring(0,1);
  var texto = mascara.substring(i)

  if (texto.substring(0,1) != saida){
            documento.value += texto.substring(0,1);
  }
};

//BOTÃO SALVAR INTERAÇÃO
$('.save').click(function(){
  $(this).addClass('btn-saved');
  $(this).text('Salvo');
});

$('.form input').on('change', retunSuccessBtn);
$('.form select').on('change', retunSuccessBtn);

function retunSuccessBtn(){
  $('.save').removeClass('btn-saved');
  $('.save').text('Salvar');
}

//SEARCH GROUP
$('#btn-search').click(function(){
  toggleWindow(this);
});

$('#btn-filter').click(function(){
  if(!isMobile){
    var text = '';
    $('#btn-search').text(text);
    $('.search-select').each(function(){
      if($(this).val() != ''){
        text += $(this).val() + ", ";
      }
    });
    $('.search-input').each(function(){
      if($(this).val() != ''){
        text += $(this).val() + ", ";
      }
    });
    text = text.slice(0,-2); //remove vircula no final do texto
    var searchIcon = $('<span>').addClass('fa fa-search');
    $('#btn-search').append(searchIcon);
    $('#btn-search').append(" "+text);
    }
    toggleWindow(this);
});

$('#btn-clear').click(function(){
  $('.search-input').val('');
  $('.search-select :nth-child(1)').prop('selected', true);
  $('#btn-search').text('');
  var searchIcon = $('<span>').addClass('fa fa-search');
  $('#btn-search').append(searchIcon);
  toggleWindow(this);
});

//Fecha o dropdown caso usuário clique fora dele
$(document).mouseup(function(e)
{
    var container = $(".dropdown-search");
    var button = $('#btn-search');
    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0 && !button.is(e.target) && button.has(e.target).length === 0)
    {
        $(container).fadeOut();
    }
});

function toggleWindow(btn){
  var toggle = "#"+$(btn).data('toggle');
  $(toggle).fadeToggle();
  // }
}

//ISOTOPE FILTERING
$('.isotope').isotope({
  // options
  itemSelector: '.iso-item',
  percentPosition: true
});

$('.filter').click(function(){
  var filtro = $(this).data('filter');
  $('.isotope').isotope({filter: filtro})
});

//STOP MODAL BUTTON focus
$('.modal').on('hidden.bs.modal', function (e) {
  e.stopImmediatePropagation();
});

//ADD CAST ITEM
$('.add-cast-item').click(function(ev){
  ev.preventDefault();
  var card = $(this).closest('.col-md-6');
  var img = $(this).data('img');
  var nome = $(this).data('nome');
  var item = $("<div class='item' data-toggle='tooltip' data-placement='top' title='"+ nome +"'><img src='img/"+ img +"'></div>");
  var link = $("<a href='#' class='remove-cast-item' data-id='"+ card.attr('id') +"'><span class='fa fa-close'></span></a>");
  //REMOVE CAST ITEM FUNCION
  $(link).click(function(){
    $(this).closest('.item').fadeOut();
    var id = '#'+$(this).data('id');
    $(id).fadeIn();
  });
  //CONTINUE ADD CAST ITEM
  $(item).append(link);
  $(item).tooltip();
  $(item).hide().fadeIn();
  $('.cast-items').append(item);
  $(card).fadeOut();
});

$('#clear-casting').click(function(){
  $('.cards .col-md-6').fadeIn();
  $('.cast-items .item').fadeOut(function(){
    $(this).remove();
  });
});

//JOB - CASTING SELECTION
$('#job-casting').on('change',function(){
  if(isMobile){
    $('#casting-list-mobile').fadeIn();
  }else{
    $('#casting-list').fadeIn();
  }
  $('#casting-msg').hide();
});

//MASKS
$('.money').mask('000.000.000.000.000,00', {reverse: true});
$('.time').mask('00:00');

//AGENDA
  $('#calendar').fullCalendar({
        events: [
        {
            title: 'Trabalho',
            start: '2017-07-04T12:30:00',
            allDay: false,
            className: 'event-blue',
            job: 'Campanha Natal',
            client: 'Agência XPTO',
            description: 'Realização do trabalho. Endereço: xyz',
            casting: 'Casting Exemplo 1',
            type: 'trabalho'
        },
        {
            title: 'Julio Mendes',
            start: '2017-07-10T15:30:00',
            allDay: false,
            className: 'event-orange',
            job: 'Campanha Natal',
            client: 'Agência XPTO',
            description: 'Casting do modelo para o cliente',
            casting: 'Casting Exemplo 1',
            type: 'modelo'
        },
        {
            title: 'Pagamento',
            start: '2017-07-12T15:30:00',
            allDay: false,
            className: 'event-red',
            job: '',
            client: 'Agência XYZ',
            description: 'Realizar pagamento do casting',
            casting: 'Casting Exemplo 2',
            type: 'pagamento'
        },
        {
            title: 'Casting',
            start: '2017-07-12T15:30:00',
            allDay: false,
            className: 'event-green',
            job: 'Casting',
            client: 'Agência XYZ',
            description: 'Teste de roupas e cenário',
            casting: 'Casting Exemplo 2',
            type: 'casting',
            modelos: 'Marcos Teixeira, Lúcia Vieira, João da Silva'
        }
        ],
        header: {center: 'today month agendaWeek agendaDay prev,next', right: ''},
        defaultTimedEventDuration: '01:00:00',
        handleWindowResize: true,
        eventStartEditable: true,
        eventClick: function(calEvent, jsEvent, view) {
          $('#titulo').text(calEvent.title);
          $('#header').attr('class','modal-header ' + calEvent.className.toString());
          var start = new Date(calEvent.start);
          $('#data').val(start.customFormat('#YYYY#-#MM#-#DD#'));
          $('#job').val(calEvent.job);
          $('#cliente').val(calEvent.client);
          $('#descricao').text(calEvent.description);
          $('#casting').val(calEvent.casting);
          $('#type').val(calEvent.type);
          $('#modalEvento').modal('show');
          return false;
        },
        eventAfterRender: function( event, element, view ){
          if(event.type == 'casting'){
            $(element).tooltip({title: event.modelos, placement: 'top'});
          }
        }
    });

    //MOBILE CALENDAR
    if(isMobile){
      $('#calendar').fullCalendar('option', 'header', {center: 'today prev,next', right: ''});
      $('#calendar').fullCalendar('option', 'height', 640);
      $('#calendar').fullCalendar('changeView', 'listMonth');
    };

    $('#novo-evento').click(function(){
      $('#titulo').text('Novo Evento');
      $('#header').attr('class','modal-header event-blue');
      $('#data').val('');
      $('#job').val('');
      $('#cliente').val('');
      $('#descricao').text('');
      $('#casting').val('');
      $('#type').val('');
    });

    $('#calendar-mini').fullCalendar({
          events: [
          {
              title: 'Campanha Natal',
              start: '2017-07-04T12:30:00',
              allDay: false,
              className: 'event-orange',
              job: 'Campanha Natal',
              client: 'Agência XPTO',
              description: 'Realização do trabalho. Endereço: xyz',
              casting: 'Casting Exemplo 1',
              type: 'trabalho'
          },
          {
              title: 'Revista Vogue',
              start: '2017-07-04T15:30:00',
              allDay: false,
              className: 'event-orange',
              job: 'Campanha Natal',
              client: 'Agência XPTO',
              description: 'Realização do trabalho. Endereço: xyz',
              casting: 'Casting Exemplo 1',
              type: 'trabalho'
          },
          {
              title: 'Teste Outdoor',
              start: '2017-07-07T15:30:00',
              allDay: false,
              className: 'event-orange',
              job: 'Campanha Natal',
              client: 'Agência XPTO',
              description: 'Realização do trabalho. Endereço: xyz',
              casting: 'Casting Exemplo 1',
              type: 'trabalho'
          }
          ],
          header: {right: 'today prev,next'},
          height: 'parent',
          defaultView: 'agendaWeek',
          duration: { days: 'visibleRange' },
          defaultTimedEventDuration: '01:00:00',
          handleWindowResize: true,
          eventStartEditable: true,
          eventClick: function(calEvent, jsEvent, view) {
            $('#titulo').text(calEvent.title);
            $('#header').attr('class','modal-header ' + calEvent.className.toString());
            var start = new Date(calEvent.start);
            $('#data').val(start.customFormat('#YYYY#-#MM#-#DD#'));
            $('#job').val(calEvent.job);
            $('#cliente').val(calEvent.client);
            $('#descricao').text(calEvent.description);
            $('#casting').val(calEvent.casting);
            $('#type').val(calEvent.type);
            $('#modalEvento').modal('show');
          return false;
      }
      });
      $('#modalAgenda').on('shown.bs.modal', function (e) {
        if(isMobile){
          $('#calendar-mini').fullCalendar('option', 'header', {center: 'today prev,next', right: ''});
          $('#calendar-mini').fullCalendar('changeView', 'listMonth');
        }else{
          $('#calendar-mini').fullCalendar('changeView', 'agendaWeek');
        }
      });

      $('#calendar-casting').fullCalendar({
            events: [
            {
                title: 'Casting Presencial',
                start: '2017-07-10T12:30:00',
                allDay: false,
                className: 'event-green',
                job: 'Campanha Natal',
                client: 'Agência XPTO',
                description: 'Casting Presencial',
                casting: 'Casting Exemplo 1',
                type: 'trabalho',
                modelos: 'Marcos Teixeira, Lúcia Vieira, João da Silva'
            },
            {
                title: 'Teste de Roupas',
                start: '2017-07-15T15:30:00',
                allDay: false,
                className: 'event-green',
                job: 'Campanha Natal',
                client: 'Agência XPTO',
                description: 'Realização do trabalho. Endereço: xyz',
                casting: 'Casting Exemplo 1',
                type: 'trabalho',
                modelos: 'Marcos Teixeira, Lúcia Vieira'
            }
            ],
            header: {right: 'today prev,next'},
            height: 'parent',
            defaultView: 'agendaWeek',
            duration: { days: 'visibleRange' },
            defaultTimedEventDuration: '02:00:00',
            handleWindowResize: true,
            eventStartEditable: true,
            eventClick: function(calEvent, jsEvent, view) {
              $('#titulo').text(calEvent.title);
              $('#header').attr('class','modal-header ' + calEvent.className.toString());
              var start = new Date(calEvent.start);
              $('#data').val(start.customFormat('#YYYY#-#MM#-#DD#'));
              $('#job').val(calEvent.job);
              $('#cliente').val(calEvent.client);
              $('#descricao').text(calEvent.description);
              $('#casting').val(calEvent.casting);
              $('#type').val(calEvent.type);
              $('#modalEvento').modal('show');
              $('#modalAgendaCasting').modal('hide');
            return false;
        },
            eventAfterRender: function( event, element, view ) {
              $(element).tooltip({title: event.modelos, placement: 'top'});
            }
        });
        $('#modalAgendaCasting').on('shown.bs.modal', function (e) {
          if(isMobile){
            $('#calendar-casting').fullCalendar('option', 'header', {center: 'today prev,next', right: ''});
            $('#calendar-casting').fullCalendar('changeView', 'listMonth');
          }else{
            $('#calendar-casting').fullCalendar('changeView', 'agendaWeek');
          }
        });



    //MOBILE NAVIGATION
    $('#mobile-nav').click(function(){
      $('.navigation').toggleClass('mobile-nav-active');
    });
    $(document).mouseup(function(e)
    {
        var container = $(".navigation");
        var button = $('#mobile-nav');
        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0 && !button.is(e.target) && button.has(e.target).length === 0)
        {
          $('.navigation').removeClass('mobile-nav-active');
        }
    });

    //MOBILE TOGGLE CASTING FILTERING
    if(isMobile){
      $('.filter-header').click(function(){
        $('.items').slideToggle();
      });
    }
});
