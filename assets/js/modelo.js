export default {
  var totalsteps = parseInt($('#total-steps').text())

  $(document).keypress(function (e) {
    if (e.which === 13) {
      stepUP($('.form-group:visible'))
      e.preventDefault()
    }
  })

  $('#btn-next').click(function () {
    stepUP($('.form-group:visible'))
  })

  $('#btn-prev').click(function () {
    stepDown($('.form-group:visible'))
  })

  function stepUP (element) {
    var jumpstep = 0
    var forward = $(element).next('.form-group')
    if (element.hasClass('resumo')) {
      $(location).attr('href', $('#btn-next').data('next'))
    }
    if (forward.length > 0) {
      if (element.attr('id') === 'passaporte') {
        if ($('input[name=passport]:checked').val() === 'nao') {
          forward = forward.next('.form-group')
          jumpstep = 1
        }
      }
      $(element).fadeOut(100)
      $(forward).slideDown(function () {
        $(forward).find('input').focus()
      })
      var step = parseInt($('#step').text())
      step = step + 1 + jumpstep
      $('#step').text(step)
      updateProgress(step)
    }
  }

  function stepDown (element) {
    var prev = $(element).prev('.form-group')
    if (prev.length > 0) {
      $(element).fadeOut(100)
      $(prev).slideDown(function () {
        $(prev).find('input').focus()
      })
      var step = parseInt($('#step').text())
      step--
      $('#step').text(step)
      updateProgress(step)
    }
  }

  $('.info').click(function () {
    $('.resumo').fadeOut(100)
    var target = $(this).data('target')
    stepUP($('#' + target).prev('.form-group'))
  })

  function updateProgress (step) {
    var bar = (step / totalsteps) * 100
    $('.progress-bar').css('width', bar + '%')
  }
}