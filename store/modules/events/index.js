import { map, findIndex } from 'lodash'
import { getData } from '~/utils/get.js'

export const state = () => {
  return {
    eventsToday: []
  }
}

export const getters = {
  eventsToday (state) {
    return map(state.eventsToday, e => {
      let hour = e.hour.split(':')
      let types = [
        { text: 'Trabalho', label: 'JOBS' },
        { text: 'Modelo', label: 'MODELS' },
        { text: 'Pagamento', label: 'PAYMENT' },
        { text: 'Casting', label: 'CASTING' },
        { text: 'Outros', label: 'OTHERS' }
      ]

      return {
        'title': e.title,
        'client': e.client.tradeName,
        'type': types[findIndex(types, ['label', e.type])].text,
        'hour': hour[0] + 'h às ' + (parseInt(hour[0]) + 1) + 'h'
        // 'hour': e.client.tradeName
      }
    })
  }
}

export const mutations = {
  FETCH_EVENTS_TODAY (state, events) {
    state.eventsToday = events
  }
}

export const actions = {
  async getEventsToday ({ commit, state }) {
    this.$axios.get('/api/v1/event/today?include=client').then(getData).then(data => {
      commit('FETCH_EVENTS_TODAY', data.data)
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
