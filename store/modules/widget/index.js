import { getData } from '~/utils/get.js'

export const state = () => {
  return {
    widgetTotals: {
      'jobsTotal': 0,
      'eventsTotal': 0,
      'modelsTotal': 0
    }
  }
}

export const getters = {
  widgetTotals (state) {
    return state.widgetTotals
  }
}

export const mutations = {
  FETCH_WIDGET_TOTALS (state, totals) {
    state.widgetTotals = totals
  }
}

export const actions = {
  async getWidgetTotals ({ commit }) {
    this.$axios.get('api/v1/widget/totals')
      .then(getData)
      .then(data => {
        commit('FETCH_WIDGET_TOTALS', data)
      })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
