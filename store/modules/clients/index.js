import serviceClient from '~/services/clients'

export const state = () => {
  return {
    clients: []
  }
}

export const getters = {
  clients (state) {
    return state.clients
  }
}

export const mutations = {
  FETCH_CLIENTS (state, clients) {
    state.clients = clients
  },
  FETCH_CLIENT (state, client) {
    state.client = client
  }
}

export const actions = {
  async getClients ({ commit, state }, payload) {
    // let data = await this.$axios.get('client/').then(getData)
    serviceClient.getClients(payload).then(data => {
      commit('FETCH_CLIENTS', data.data)
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
