import actions from './actions'
import mutations from './mutations'

const state = () => {
  return {
    castings: [],
    casting: { id: 0, description: '', client: {}, models: [], events: [] }
  }
}

const getters = {
  castings (state) {
    return state.castings
  },
  casting (state) {
    return state.casting
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
