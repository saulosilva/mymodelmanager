import { unset } from 'lodash'

export default {
  FETCH_CASTINGS (state, castings) {
    state.castings = castings
  },
  FETCH_CASTING_SELECT (state, casting) {
    if (casting.models.data) {
      let models = casting.models.data
      unset(casting, 'models')
      unset(casting, 'meta')
      casting.models = models
    }

    if (casting.events.data) {
      let events = casting.events.data
      casting.events = events
    }

    state.casting = casting
  },
  RESET_CASTING_SELECT (state, casting) {
    state.casting = { id: 0, description: '', client: {}, models: [], events: [] }
  },
  FETCH_CASTING_DUPLICATE (state, casting) {
    if (casting.models.data) {
      let models = casting.models.data
      unset(casting, 'models')
      unset(casting, 'meta')
      casting.models = models
    }

    casting.id = 'duplicate'

    state.casting = casting
  },
  CASTING_ADD_MODEL (state, model) {
    let { id, artisticName, photo } = model
    var check = state.casting.models.indexOf({ id, artisticName, photo })
    if (check === -1) {
      state.casting.models.push({ id, artisticName, photo })
    }
  },
  CASTING_REMOVE_MODEL (state, model) {
    var check = state.casting.models.indexOf(model)
    if (check > -1) {
      state.casting.models.splice(check, 1)
    }
  }
}
