/* eslint-disable camelcase */
import castingService from '~/services/castings'
// import { find } from 'lodash'

export default {
  getCastings ({ commit, state }, payload) {
    castingService.getCastings(payload).then(data => {
      commit('FETCH_CASTINGS', data.data)
    })
  },
  getCasting ({ commit, state }, payload) {
    let id = payload.id
    castingService.getCasting(id).then(data => {
      commit('FETCH_CASTING_SELECT', data)
    })
  },
  duplicateCasting ({ commit, state }, payload) {
    let id = payload.id
    castingService.getCasting(id).then(data => {
      commit('FETCH_CASTING_DUPLICATE', data)
    })
  },
  addCastingModel ({ commit, state }, payload) {
    let model = payload
    commit('CASTING_ADD_MODEL', model)
  },
  removeCastingModel ({ commit, state }, payload) {
    let model = payload
    commit('CASTING_REMOVE_MODEL', model)
  },
  // getModel ({ commit, state }, payload) {
  //   let id = payload.id
  //   let model = find(state.models, r => r.id === id)
  //   if (model === undefined) {
  //     model = modelsService.getModel(id).then(data => {
  //       commit('FETCH_MODEL_SELECT', data.data)
  //     })
  //     return false
  //   }
  //   commit('FETCH_MODEL_SELECT', model)
  // },
  // createModel ({ commit, state }, payload) {
  //   modelsService.create(payload).then(res => {
  //     let data = res.data
  //     localStorage.cpf = data.cpf
  //     commit('FETCH_MODEL_SELECT', data)
  //   })
  // },
  // updateModel ({ commit }, payload) {
  //   modelsService.updateModel(payload.id, payload).then(res => {
  //     let data = res.data
  //     // localStorage.cpf = data.cpf
  //     commit('FETCH_MODEL_SELECT', data)
  //   })
  // },
  async 'casting/destroy' ({ dispatch, state }, payload) {
    await castingService.destroy(payload.id)
    dispatch('getCastings')
  }
}
