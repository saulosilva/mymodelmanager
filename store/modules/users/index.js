import serviceUsers from '~/services/user'

export const state = () => {
  return {
    users: [],
    roles: [
      { 'name': 'Financeiro', 'role': 'finance' },
      { 'name': 'Casting', 'role': 'casting' },
      { 'name': 'New Face', 'role': 'newface' },
      { 'name': 'Admin', 'role': 'admin' }
    ]
  }
}

export const getters = {
  users (state) {
    return state.users
  },
  roles (state) {
    return state.roles
  }
}

export const mutations = {
  FETCH_USERS (state, users) {
    state.users = users
  }
}

export const actions = {
  async getUsers ({ commit, state }, payload) {
    serviceUsers.getUsers(payload).then(data => {
      commit('FETCH_USERS', data.data)
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
