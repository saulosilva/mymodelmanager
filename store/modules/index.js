import users from './users'
import casting from './casting'
import agency from './agency'
import models from './models'
import steps from './steps'
import wizard from './wizard'
import clients from './clients'
import jobs from './jobs'
import events from './events'
import widget from './widget'

export default {
  users,
  agency,
  models,
  steps,
  casting,
  wizard,
  clients,
  jobs,
  events,
  widget
}
