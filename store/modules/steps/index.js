import mutations from './mutations'

const state = () => {
  return {
    step: 1,
    stepCompleted: []
  }
}

const getters = {
  step (state) {
    return state.step
  },
  completedStep: (state) => (step) => {
    return state.stepCompleted.indexOf(step) > -1
  },
  completed (state) {
    return state.stepCompleted.length === 8
  }
}

export default {
  state,
  getters,
  mutations
}
