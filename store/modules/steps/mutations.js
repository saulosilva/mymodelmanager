export default {
  SET_STEP (state, step) {
    state.step = step
  },
  'route/ROUTE_CHANGED' (state, payload) {
    let name = payload.to.name

    if (payload.to.name !== null) {
      if (payload.to.name.indexOf('model-step') > -1) {
        let step = parseInt(name.replace('model-step', ''))

        let check = state.stepCompleted.indexOf(step)
        state.step = step

        if (check === -1) {
          state.stepCompleted.push(step)
        }
      }
    }
  }
}
