import modelsService from '~/services/models'
import modelReset from './model'

export default {
  getModels ({ commit, state }, payload) {
    modelsService.getModels(payload).then(data => {
      commit('FETCH_MODELS', data.data)
    })
  },
  getModel ({ commit, state }, payload) {
    let id = payload.id
    // restura valores iniciais antes do preenchimento do getModel
    commit('FETCH_MODEL_SELECT', modelReset)
    modelsService.getModel(id).then(data => {
      commit('FETCH_MODEL_SELECT', data)
    })
  },
  createModel ({ commit, state }, payload) {
    modelsService.create(payload).then(res => {
      let data = res.data
      localStorage.cpf = data.cpf
      commit('FETCH_MODEL_SELECT', data)
    })
  },
  updateModelLink ({ commit }, payload) {
    let id = localStorage.idModel
    modelsService.updateLink(id, payload).then(data => {
      commit('FETCH_MODEL_SELECT', data)
    })
  },
  async 'model/destroy' ({ dispatch, state }, payload) {
    await modelsService.destroy(payload.id)
    dispatch('getWidgetTotals')
    dispatch('getModels')
  }
}
