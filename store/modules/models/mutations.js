// const addZero = (value) => {
//   if (value < 10) {
//     value = '0' + value
//   }
//   return value
// }
export default {
  FETCH_MODELS (state, models) {
    state.models = models
  },
  FETCH_MODEL_SELECT (state, model) {
    // if (model.model) {
    //   let birthDate = model.model.birthDate
    //   model.model.birthDate = birthDate[0] + '-' + addZero(birthDate[1]) + '-' + addZero(birthDate[2])
    // }
    state.modelSelected = { ...state.modelSelected, ...model }
  },
  FETCH_MODEL_PHOTO (state, photo) {
    state.modelSelected.photo = photo
  },
  FETCH_MODEL_ADDRESS (state, adresses) {
    state.modelSelected.modelAddress = adresses
  },
  FETCH_MODEL_CHARACTERS (state, characters) {
    state.modelSelected.modelCharacteristics = characters
  },
  FETCH_MODEL_EXPERIENCE (state, experience) {
    state.modelSelected.modelExperience = experience
  },
  FETCH_MODEL_MEASURES (state, measures) {
    state.modelSelected.modelMeasurements = measures
  },
  FETCH_MODEL_REPRESENTATIVE (state, representative) {
    state.modelSelected.modelRepresentative = representative
  },
  FETCH_MODEL_BANKS (state, banks) {
    state.modelSelected.modelBankAccount = banks
  },
  FETCH_MODEL_EDUCATION (state, education) {
    state.modelSelected.modelEducation = education
  },
  FETCH_MODEL_COMPOSITES (state, composites) {
    state.modelSelected.modelComposites = composites
  }
}
