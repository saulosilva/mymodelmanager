import actions from './actions'
import mutations from './mutations'
import model from './model'

const state = () => {
  return {
    models: [],
    modelSelected: model
  }
}

const getters = {
  models (state) {
    return state.models
  },
  modelCount (state) {
    return state.models.length
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
