let modelAddress = { street: '', additionalInfo: '', number: '', cityBlock: '', city: '', zipCode: '', state: '', country: 'Brasil' }
let modelCharacteristics = { skinColor: '', hairColor: '', hairLength: '', hairType: '', hairUnnatural: '', eyesColor: '', ethnicity: '' }
let modelMeasurements = { height: 0.00, weight: 0, shirt: 0, suit: 0, shoes: 0, dummy: 0, soutien: 0, bust: 0, chest: 0, waist: 0, hip: 0, thighs: 0 }
let modelEducation = { scholarity: 'NONE', actorActress: false, drt: '', presenter: false, singer: false, dancer: false, ceremonyMaster: false, language: [] }
let modelExperience = { hasProfessionalMaterial: false, hasPhotosAndVideos: false, otherAgencies: '', availability: [], internacionalExperiences: '', employed: false, companyName: '', workingArea: '', skills: '' }
let modelBankAccount = { number: '', agency: '', bankCode: '', bankName: '', holder: true, type: 'CC', holderName: '', document: '' }
let modelRepresentative = { cpf: '', fullName: '', rg: '' }
let modelComposites = []

let model = {
  id: 0,
  fullName: '',
  artisticName: '',
  gender: 'MALE',
  birthDate: '1900-01-01',
  rg: '',
  cpf: '',
  hasPassport: false,
  passaportNumber: '',
  facebook: '',
  email: '',
  photo: '',
  isOpen: false,
  isEnabled: false,
  modelComposites: modelComposites,
  modelBankAccount: modelBankAccount,
  modelExperience: modelExperience,
  modelAddress: modelAddress,
  modelCharacteristics: modelCharacteristics,
  modelMeasurements: modelMeasurements,
  modelEducation: modelEducation,
  modelRepresentative: modelRepresentative
}

export default model
