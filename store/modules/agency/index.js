import { getData } from '~/utils/get.js'

const state = () => {
  return {
    agency: {
      tradingName: '',
      companyName: '',
      document1: '',
      document2: '',
      site: '',
      email: '',
      logo: '',
      agencyAddress: {
        city: '',
        number: '',
        country: 'Brasil',
        state: '',
        street: '',
        additionalInfo: '',
        cityBlock: '',
        zipCode: '',
        phone1: '',
        phone2: '',
        phone3: '',
        contactName: '',
        contactPosition: ''
      },
      agencyBankAccount: {
        number: '',
        agency: '',
        bankCode: '',
        bankName: '',
        type: 'CC'
      }
    }
  }
}

const getters = {
  agency (state) {
    return state.agency
  }
}

const mutations = {
  FETCH_AGENCY (state, agency) {
    state.agency = agency
  }
}

const actions = {
  getAgency ({ commit, state }) {
    return this.$axios.get('/api/v1/agency')
      .then(getData)
  },
  async updateAgency ({ commit, state }, payload) {
    let data = await this.$axios.put('/api/v1/agency', payload).then(getData)
    commit('FETCH_AGENCY', data)
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
