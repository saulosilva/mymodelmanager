/*
    Este modulo esta voltado para o cadastro de modelos utilizando o Wizard.
*/
import actions from './actions'
import mutations from './mutations'
import model from './model'
import moment from 'moment'

const state = () => {
  return {
    model: model
  }
}

const getters = {
  hasOfAge (state) {
    let birthDate = state.model.model.birthDate
    if (birthDate === '1900-01-01') {
      return false
    }

    var date = moment()
    var ageDate = moment(birthDate)
    var a = moment([date.year(), 1])
    var b = moment([ageDate.year(), 0])

    return a.diff(b, 'years') >= 18
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
