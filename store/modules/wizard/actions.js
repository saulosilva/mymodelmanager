import wizardService from '~/services/wizard'
// import modelDefault from './model'

export default {
  loadingModel ({ commit }, id) {
    if (id) {
      return wizardService.getModel(id).then(data => {
        localStorage.idModel = data.code
        commit('FETCH_MODEL', data)
      }, (err) => {
        if (err.response.status === 404) {
          localStorage.idModel = ''
          window.location = '/404'
        }
      })
    }
  },
  getModelToken ({ commit, state }, payload) {
    let id = payload.id
    // restura valores iniciais antes do preenchimento do getModel
    wizardService.getModel(id).then(data => {
      commit('FETCH_SELECT', data)
    }, (err) => {
      if (err.response.status === 404) {
        localStorage.idModel = ''
        window.location = '/404'
      }
    })
  },
  updateModelWizard ({ commit }, payload) {
    return wizardService.updateModel(localStorage.idModel, payload).then(data => {
      commit('FETCH_MODEL', data)
    })
  },
  saveRelationWizard ({ dispatch, commit, state, getters }, payload) {
    let relation = payload.relation
    let form = payload.data
    let id = localStorage.idModel

    switch (relation) {
      case 'adresses': {
        // atualiza um endereco
        return wizardService.saveAddress(id, form).then(data => {
          commit('FETCH_ADDRESS', data.model.modelAddress)
        })
        // break
      }
      case 'representative': {
        return wizardService.saveRepresentative(id, form).then(data => {
          commit('FETCH_REPRESENTATIVE', data.model.modelRepresentative)
        })
      }
      case 'characteristics': {
        // atualiza um caracteristicas
        return wizardService.saveCharacteristics(id, form).then(data => {
          commit('FETCH_CHARACTERISTICS', data.model.modelCharacteristics)
        })
        // break
      }
      case 'measures': {
        // atualiza um medida
        return wizardService.saveMeasures(id, form).then(data => {
          commit('FETCH_MEASURES', data.model.modelMeasurements)
        })
        // break
      }
      case 'education': {
        // atualiza um graduacao
        return wizardService.saveEducation(id, form).then(data => {
          commit('FETCH_EDUCATION', data.model.modelEducation)
        })
        // break
      }
      case 'experience': {
        // atualiza um medida
        return wizardService.saveExperience(id, form).then(data => {
          commit('FETCH_EXPERIENCE', data.model.modelExperience)
        })
        // break
      }
      case 'banks': {
        // atualiza um medida
        return wizardService.saveBank(id, form).then(data => {
          commit('FETCH_BANKS', data.model.modelBankAccount)
        })
        // break
      }
    }
  },
  async 'wizard/success' () {
    await wizardService.success(localStorage.idModel)
  }
}
