
export default {
  FETCH_MODEL (state, model) {
    state.model = { ...state.model, ...model }
  },
  FETCH_ADDRESS (state, adresses) {
    state.model.model.modelAddress = adresses
  },
  FETCH_CHARACTERISTICS (state, characters) {
    state.model.model.modelCharacteristics = characters
  },
  FETCH_EXPERIENCE (state, experience) {
    state.model.model.modelExperience = experience
  },
  FETCH_MEASURES (state, measures) {
    state.model.model.modelMeasurements = measures
  },
  FETCH_REPRESENTATIVE (state, representative) {
    state.model.model.modelRepresentative = representative
  },
  FETCH_BANKS (state, banks) {
    state.model.model.modelBankAccount = banks
  },
  FETCH_EDUCATION (state, education) {
    state.model.model.modelEducation = education
  }
}
