import model from '~/store/modules/models/model'

let modelDefault = {
  id: '',
  email: '',
  status: '',
  step: 0,
  creationDate: [],
  model: model
}

export default modelDefault
