/*
    Este modulo esta voltado para o cadastro de modelos utilizando o Wizard.
*/
import serviceJobs from '~/services/jobs/jobs'
import { unset } from 'lodash'

const state = () => {
  return {
    job: {},
    jobs: [],
    filterJobs: [],
    costs: []
  }
}

const getters = {
  totalJobs (state) {
    return state.jobs.length
  },
  filterJobs (state) {
    return state.filterJobs
  },
  hasJob (state) {
    return state.job
  },
  costsModel (state) {
    // lodash
    return (modelId, castingId) => window._.filter(state.costs, item => {
      return (item.modelId === modelId) && (item.castingId === castingId)
    })
  }
}

export const mutations = {
  FETCH_JOBS (state, jobs) {
    state.jobs = jobs
  },
  FETCH_JOB (state, job) {
    state.job = job
  },
  FILTER_JOBS (state, search) {
    state.filterJobs = state.jobs
  },
  FETCH_COSTS (state, costs) {
    state.costs = costs
  },
  FETCH_JOBS_DUPLICATE (state, job) {
    if (job.costs) {
      unset(job, 'costs')
      unset(job, 'meta')
      job.costs = []
    }

    job.status = 'WAITING_APPROVAL'
    job.id = 'duplicate'

    state.job = job
  }
}

export const actions = {
  async getJobs ({ commit, state }, payload) {
    serviceJobs.getJobs(payload).then(data => {
      commit('FETCH_JOBS', data.data)
      commit('FILTER_JOBS', { filter: false })
    })
  },
  async getCosts ({ commit, state }, jobId) {
    serviceJobs.getCosts(jobId).then(data => {
      commit('FETCH_COSTS', data)
    })
  },
  async 'jobs/destroy' ({ dispatch, state }, payload) {
    await serviceJobs.destroy(payload.id)
    dispatch('getWidgetTotals')
    dispatch('getJobs')
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
