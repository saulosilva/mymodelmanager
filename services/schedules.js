import http from '~/plugins/http'
import { getData } from '~/utils/get.js'

export default {
  async getSchedule (id) {
    return http.get('/api/v1/event/' + id).then(getData)
  },
  async create ({ jobId, clientId, castingId, date, hour, type, description, title, job, models }) {
    return http.post('/api/v1/event/', { jobId, clientId, castingId, date, hour, type, description, title, job, models }).then(getData)
  },
  async update (id, data) {
    return http.put('/api/v1/event/' + id, data).then(getData)
  },
  destroy (id) {
    return http.delete('/api/v1/event/' + id).then(getData)
  }
}
