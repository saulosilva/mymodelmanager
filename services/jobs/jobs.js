import http from '~/plugins/http'
import { getData } from '~/utils/get.js'

export default {
  async getJobs (filters = { client: '', title: '', status: '' }) {
    let search = ''
    if (filters.title !== '') {
      search += 'title:' + filters.title
    }

    if (filters.client !== '') {
      if (filters.title !== '') {
        search += ';'
      }
      search += 'client.trade_name:' + filters.client
    }

    if (filters.status !== '') {
      if (filters.title !== '' || filters.client !== '') {
        search += ';'
      }
      search += 'status:' + filters.status
    }

    return http.get('/api/v1/jobs/?include=client&search=' + search).then(getData)
  },
  async getJob (id) {
    return http.get('/api/v1/jobs/' + id + '/?include=client,casting,costs').then(getData)
  },
  async getCosts (id) {
    return http.post('/api/v1/jobs/' + id + '/model-costs').then(getData)
  },
  async create (data) {
    return http.post('/api/v1/jobs', data).then(getData)
  },
  async update (id, data) {
    return http.put('/api/v1/jobs/' + id, data).then(getData)
  },
  async destroy (id) {
    return http.delete('/api/v1/jobs/' + id).then(getData)
  },
  async duplicate (id) {
    return http.post('/api/v1/jobs/' + id + '/duplicate').then(getData)
  }
}
