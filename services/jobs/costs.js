import http from '~/plugins/http'
import { getData } from '~/utils/get.js'

export default {
  async getCosts () {
    return http.get('/api/v1/costs').then(getData)
  },
  async getCost (id) {
    return http.get('/api/v1/jobs/' + id + '/model-costs').then(getData)
  },
  async create (data) {
    return http.post('/api/v1/jobs/model-costs', data).then(getData)
  },
  async edit (id, data) {
    return http.put('/api/v1/costs', data).then(getData)
  },
  async destroyExpenses (id) {
    return http.delete('/api/v1/costs/' + id).then(getData)
  }
}
