import http from '~/plugins/http'
import { getData } from '~/utils/get.js'

export default {
  async getUsers (filters = {}) {
    return http.get('/api/v1/users', { params: filters }).then(getData)
  },
  async getUser (id) {
    return http.get('/api/v1/users/' + id).then(getData)
  },
  async create (data) {
    return http.post('/api/v1/users', data).then(getData)
  },
  async update (id, data) {
    return http.put('/api/v1/users/' + id, data).then(getData)
  },
  changeStatus (id, status) {
    return http.put('/api/v1/users/' + id + '/' + status).then(getData)
  }
}
