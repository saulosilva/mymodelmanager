import http from '~/plugins/http'
import { getData } from '~/utils/get.js'

export default {
  async getClients (filters = {}) {
    return http.get('/api/v1/clients', { params: filters }).then(getData)
  },
  async getClient (id) {
    return http.get('/api/v1/clients/' + id).then(getData)
  },
  async create (data) {
    return http.post('/api/v1/clients', data).then(getData)
  },
  async update (id, data) {
    return http.patch('/api/v1/clients/' + id, data).then(getData)
  },
  async changeStatus (id, status) {
    return http.put('/api/v1/clients/' + id + '/' + status).then(getData)
  }
}
