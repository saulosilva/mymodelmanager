/* eslint-disable camelcase */
import http from '~/plugins/http'
import { getData } from '~/utils/get.js'

export default {
  async getModels (filters = {}) {
    return http.get('/api/v1/models', { params: filters }).then(getData)
  },
  async getModel (id) {
    return http.get('/api/v1/models/' + id).then(getData)
  },
  create (data) {
    return http.post('/api/v1/models', data).then(getData)
  },
  async update (id, data) {
    return http.put('/api/v1/models/' + id, data).then(getData)
  },
  async destroy (id) {
    return http.delete('/api/v1/models/' + id).then(getData)
  },
  async updatePhoto (id, photo) {
    return http.put('/api/v1/models/' + id + '/photo', { photo: photo }).then(getData)
  },
  // save address
  async saveAddress (modelId, data) {
    return http.put('/api/v1/models/' + modelId + '/address', data)
      .then(getData)
  },
  // save characteristics
  async saveCharacteristics (modelId, data) {
    return http.put('/api/v1/models/' + modelId + '/characteristics', data)
      .then(getData)
  },
  async saveMeasures (modelId, data) {
    return http.put('/api/v1/models/' + modelId + '/measurements', data)
      .then(getData)
  },
  async saveEducation (modelId, data) {
    return http.put('/api/v1/models/' + modelId + '/education', data)
      .then(getData)
  },
  async saveBankAccount (modelId, data) {
    return http.put('/api/v1/models/' + modelId + '/bank-account', data)
      .then(getData)
  },
  async saveExperience (modelId, data) {
    return http.put('/api/v1/models/' + modelId + '/experience', data)
      .then(getData)
  },
  async saveRepresentative (modelId, data) {
    return http.put('/api/v1/models/' + modelId + '/representative', data)
      .then(getData)
  },
  async saveComposite (modelId, data) {
    return http.put('/api/v1/models/' + modelId + '/composites', data)
      .then(getData)
  },
  async enableModel (modelId, status) {
    return http.post('/api/v1/models/' + modelId + '/' + status)
      .then(getData)
  },
  async approveModel (modelId, status) {
    return http.post('/api/v1/models/approval/' + modelId + '/' + status)
      .then(getData)
  },
  // model links routes
  async sendLink (data) {
    return http.post('/api/v1/models/link', data).then(getData)
  },
  async getModelId (id) {
    return http.get('/api/v1/models/link/' + id).then(getData)
  },
  async updateLink (id, data) {
    return http.put('/api/v1/models/link', data, {
      headers: { 'link-token': id }
    }).then(getData)
  }
}
