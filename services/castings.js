/* eslint-disable camelcase */
import http from '~/plugins/http'
import { getData } from '~/utils/get.js'

export default {
  getCastings (filters = { description: '', client: '', status: '' }) {
    let search = ''
    if (filters.description !== '') {
      search += 'description:' + filters.description
    }

    if (filters.client !== '') {
      if (filters.description !== '') {
        search += ';'
      }
      search += 'client.trade_name:' + filters.client
    }

    if (filters.status !== '') {
      if (filters.description !== '' || filters.client !== '') {
        search += ';'
      }
      search += 'status:' + filters.status
    }

    return http.get('/api/v1/casting?include=job,models,client,user,events&search=' + search).then(getData)
  },
  async getCasting (id) {
    return http.get('/api/v1/casting/' + id + '?include=job,models,client,user,events').then(getData)
  },
  async getModelsFilter (search = {}) {
    let params = { ...search }
    return http.post('/api/v1/casting/filter', params).then(getData)
  },
  create (data) {
    return http.post('/api/v1/casting', data).then(getData)
  },
  async update (id, data) {
    return http.put('/api/v1/casting/' + id, data).then(getData)
  },
  async destroy (id) {
    return http.delete('/api/v1/casting/' + id).then(getData)
  },
  async duplicate (id) {
    return http.post('/api/v1/casting/duplicate/' + id).then(getData)
  }
  // async update (id, data) {
  //   return http.put('castings/' + id, data).then(getData)
  // },
  // async create ({ education, actor, drt_number, presenter, vocalist, dancer, master_ceremony, language }) {
  //   return http.post('castings', { education, actor, drt_number, presenter, vocalist, dancer, master_ceremony, language }).then(getData)
  // }
}
