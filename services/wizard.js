import http from '~/plugins/http'
import { getData } from '~/utils/get.js'

export default {
  async getModel (id) {
    return http.get('/api/v1/model/link/' + id).then(getData)
  },
  async updateModel (id, data) {
    return http.put('/api/v1/model/link/' + id, data).then(getData)
  },
  async saveAddress (id, data) {
    return http.put('/api/v1/model/link/' + id + '/address', data).then(getData)
  },
  async saveCharacteristics (id, data) {
    return http.put('/api/v1/model/link/' + id + '/characteristics', data).then(getData)
  },
  async saveMeasures (id, data) {
    return http.put('/api/v1/model/link/' + id + '/measurements', data).then(getData)
  },
  async saveEducation (id, data) {
    return http.put('/api/v1/model/link/' + id + '/education', data).then(getData)
  },
  async saveExperience (id, data) {
    return http.put('/api/v1/model/link/' + id + '/experience', data).then(getData)
  },
  async saveBank (id, data) {
    return http.put('/api/v1/model/link/' + id + '/bank-account', data).then(getData)
  },
  async saveRepresentative (id, data) {
    return http.put('/api/v1/model/link/' + id + '/representative', data).then(getData)
  },
  async success (id) {
    return http.put('/api/v1/model/link/' + id + '/success').then(getData)
  }
}
