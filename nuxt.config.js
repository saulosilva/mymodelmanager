const webpack = require('webpack')
// const baseURL = 'http://localhost:3000'
// const apiURL = 'http://api.mymodelmanager.local'
const baseURL = 'https://mymmvue.herokuapp.com'
const apiURL = 'https://mymm.s10design.com.br'

module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'My Model Manager',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Montserrat|Raleway' }
    ]
  },
  css: [
    'bootstrap/dist/css/bootstrap.css',
    '@/assets/css/bootstrap-slider.css',
    '@/assets/css/fullcalendar.css',
    '@/assets/css/global.css'
  ],
  env: {
    // BASE_URL: 'https://mymmvue.herokuapp.com'
    BASE_URL: baseURL
  },
  plugins: [
    '~/plugins/vue-swal',
    {src: '~plugins/vee-validate.js', ssr: true},
    {src: '~plugins/vue-picture-input', ssr: false},
    '~plugins/vue-router',
    '~plugins/vue-focus',
    '~plugins/axios-port',
    '~plugins/uiv',
    '~plugins/vueisotope',
    '~plugins/vue-moment',
    '~plugins/vuex-router-sync.js',
    '~plugins/vue-clipboard2',
    '~plugins/awesome-mask',
    '~plugins/vue-ls',
    { src: '~plugins/vuejs-paginate', ssr: false },
    { src: '~plugins/vue-fullcalendar', ssr: false },
    '~plugins/vue-bootstrap-slider.js',
    '~plugins/filters',
    '~plugins/jquery.js',
    '~plugins/models.js',
    '~plugins/bootstrap.js',
    '~plugins/global.js'
  ],
  modules: [
    '@nuxtjs/font-awesome',
    '@nuxtjs/auth',
    '@nuxtjs/axios',
    '@nuxtjs/proxy',
    '@nuxtjs/toast'
  ],
  toast: {
    theme: 'primary',
    position: 'bottom-center',
    duration: 2000
  },
  axios: {
    baseURL: apiURL,
    proxy: true,
    retry: { retries: 3 }
  },
  // Default Values
  auth: {
    redirect: {
      login: '/login',
      logout: '/',
      callback: '/',
      home: '/dashboard',
      user: '/'
    },
    strategies: {
      local: {
        endpoints: {
          login: { url: '/api/v1/auth', propertyName: 'access_token' },
          logout: { url: '/api/v1/logout', method: 'GET' },
          user: { url: '/api/v1/user/authenticated', method: 'get', propertyName: 'user' }
        },
        storageTokenName: 'token'
        // tokenType: 'Bearer'
      }
    },
    // plugins: [ '~/plugins/auth.js' ]
  },
  proxy: {
    '/api/v1/': apiURL,
    '/img/cache/': apiURL
  },
  /*
  ** Customize the progress bar color
  */
  // loading: { color: '#3B8070', height: '5px' },
  loading: '~/components/loading.vue',
  /*
  ** Build configuration
  */
  build: {
    watch: ['./components'],
    vendor: [
      'jquery',
      'bootstrap',
      'vue-swal',
      'vee-validate',
      'vue-picture-input',
      'awesome-mask',
      'uiv',
      'vue-moment'
    ],
    plugins: [
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
        _: 'lodash',
        'window._': 'lodash'
      })
    ],
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      config.resolve.alias['masonry'] = 'masonry-layout'
      config.resolve.alias['isotope'] = 'isotope-layout'

      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
